#version 400
layout(location=0) in vec4 VertexPos;
layout(location=1) in vec4 VertexNormal;
layout(location=2) in vec2 VertexTexcoord;
layout(location=3) in vec2 VertexTexcoord2;
layout(location=4) in vec2 VertexTexcoord3;

out vec3 Position;
out vec3 Normal;
out vec2 Texcoord;
out vec3 Tangent;
out vec3 Bitangent;

uniform mat4 ModelMat;
uniform mat4 ModelViewProjMat;

void main()
{
    Position = (ModelMat * VertexPos).xyz;
    Normal = (ModelMat * vec4(VertexNormal.xyz,0)).xyz;
    Texcoord = VertexTexcoord;

    Tangent = vec3(VertexTexcoord2.x, VertexTexcoord2.y, 1 - abs(VertexTexcoord2.x) - abs(VertexTexcoord2.y) );
    Bitangent = cross(Tangent, Normal);
    Tangent = (ModelMat * vec4(Tangent.xyz,0)).xyz;
    Bitangent = (ModelMat * vec4(Bitangent.xyz,0)).xyz;

    gl_Position = ModelViewProjMat * VertexPos;
}
