#version 400

const int MAX_LIGHTS=64;

in vec3 Position;
in vec3 Normal;
in vec2 Texcoord;

//AUFGABE3
in vec3 Tangent;
in vec3 Bitangent;

out vec4 FragColor;

uniform vec3 LightPos;
uniform vec3 LightColor;

uniform vec3 EyePos;
uniform vec3 DiffuseColor;
uniform vec3 SpecularColor;
uniform vec2 FogDensity;
uniform vec3 FogColor;
uniform vec3 AmbientColor;
uniform float SpecularExp;
uniform sampler2D DiffuseTexture;
//AUFGABE3
uniform sampler2D NormalTexture;

struct Light
{
	int Type;
	vec3 Color;
	vec3 Position;
	vec3 Direction;
	vec3 Attenuation;
	vec3 SpotRadius;
	int ShadowIndex;
};

uniform Lights
{
	int LightCount;
	Light lights[MAX_LIGHTS];
};

float sat( in float a)
{
    return clamp(a, 0.0, 1.0);
}

float getFogFactor(float d)
{
    float FogMax = FogDensity.y;
    float FogMin = FogDensity.x;

    if (d>=FogMax) return 1;
    if (d<=FogMin) return 0;

    return 1 - (FogMax - d) / (FogMax - FogMin);
}

void main()
{
    vec4 DiffTex = texture( DiffuseTexture, Texcoord);
    if(DiffTex.a <0.3f) discard;
    vec3 N = normalize(Normal);
    vec3 E = normalize(EyePos-Position);

    vec3 DiffuseComponent = vec3(0,0,0);
	vec3 SpecularComponent = vec3(0,0,0);
    vec3 Color = vec3(0,0,0);

    //Aufgabe3
    mat3 matrix = mat3(Tangent, -Bitangent, Normal);
    vec3 normalNormalMap = texture(NormalTexture, Texcoord).rgb;

    normalNormalMap = vec3((normalNormalMap.r*2) -1, (normalNormalMap.g*2) -1, (normalNormalMap.b*2)-1);

    N = normalize(matrix * normalNormalMap);


	for(int i=0; i<LightCount;i++){
        float intensity = 1.f;
        float dist = length(lights[i].Position - Position);
        vec3 L = normalize(lights[i].Position-Position);
        vec3 R = reflect(-L,N);
        vec3 H = normalize(L+E);

        // Punktlichtintensitätsberechnungsformeldurchführung
        if(lights[i].Type == 0){
            intensity = 1.0f / (lights[i].Attenuation.x + lights[i].Attenuation.y * dist + lights[i].Attenuation.z * pow(dist,2));
        // Direktional licht
        }else if(lights[i].Type == 1){
            intensity = 1.0f;
            L = - lights[i].Direction;
        }
        else if(lights[i].Type == 2){
            float omega = acos(dot(normalize(lights[i].Direction),normalize(-L)));
            float gammaI = lights[i].SpotRadius[0];
		    		float gamma0 = lights[i].SpotRadius[1];

            intensity = 1.0f / (lights[i].Attenuation.x + lights[i].Attenuation.y * length(L) + lights[i].Attenuation.z * pow(length(L),2));
            intensity *= 1.0f - sat((omega - gammaI)/(gamma0-gammaI));
        }
        Color= lights[i].Color * intensity;
        DiffuseComponent += Color * sat(dot(N,L));
        SpecularComponent += Color * pow(sat(dot(N, H)), SpecularExp);
    }


		//Fog:
		//vec4 FogColor = vec4(0.5f,0.5f,0.8f,1.0f);
		float d = length(EyePos-Position);
    float alpha = getFogFactor(d);

		//Outline Shader
		float cosAngle = 1- abs(dot(N,E));

    DiffuseComponent *= DiffuseColor;
    SpecularComponent *= SpecularColor;
		vec4 OriginalColor = vec4((DiffuseComponent + AmbientColor)*DiffTex.rgb + SpecularComponent ,DiffTex.a);
		vec4 Outline = vec4(vec3(1,1,1) * pow(cosAngle,8),0.5f);
		FragColor = mix(OriginalColor + Outline, vec4(FogColor, 1.0f), alpha);
    //FragColor = vec4(N,1);
}
