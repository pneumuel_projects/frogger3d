#ifndef Strip_h
#define Strip_h

#include "BaseModel.h"
#include "Aabb.h"
#include "Model.h"
#include "PhongShader.h"
#include <list>

class Strip : public Model
{
public:
	Strip();
	Strip(const char* blockPath, Vector position);
	virtual ~Strip();

	virtual bool checkCollision(Vector position, Vector boundingBox) const;
	virtual void update(float deltaTime);
	virtual void setPosition(Vector position);
	virtual AABB getBoundingBox() { return this->boundingBox(); }
	virtual std::string& getPath() { return path; }
	void getModels(std::list<BaseModel*>& list);
protected:

private:
	std::string path;
};

#endif /* Strip */
