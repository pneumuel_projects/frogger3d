#ifndef __SimpleRayTracer__vector__
#define __SimpleRayTracer__vector__

#include <iostream>

class Vector
{
public:
	float X;
	float Y;
	float Z;

	Vector(float x, float y, float z);
	Vector(Vector vec ,float maginitude);
	Vector();

	float dot(const Vector& v) const;
	Vector cross(const Vector& v) const;
	Vector operator+(const Vector& v) const;
	Vector operator-(const Vector& v) const;
	Vector& operator+=(const Vector& v);
	Vector operator*(float c) const;
	Vector operator/(float val) const;
	Vector operator-() const;
	Vector& normalize();
	float length() const;
	float lengthSquared() const;
	Vector reflection(const Vector& normal) const;
	float triangleS(const Vector &d, const Vector &a, const Vector &b, const Vector &c) const;
	bool triangleIntersectionGivenS(const Vector &d, const Vector &a, const Vector &b,
		const Vector &c, float s) const;
	bool triangleIntersection(const Vector& d, const Vector& a, const Vector& b,
		const Vector& c, float& s) const;
	float flaecheDreieck(const Vector& a, const Vector& b, const Vector& c) const;
	Vector unit() const;
	void rotate(const Vector&, double);

	float magnitude() const;
};

#endif /* defined(__SimpleRayTracer__vector__) */
