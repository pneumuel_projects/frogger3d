#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

/*Inspiration von github.com/usmanshaahid/ParticleSystem
wurde aber stark abgeandert*/

#include <vector>
#include "ParticleGenerator.h"
#include "Matrix.h"

class ParticleSystem
{
	//Liste von Particel
	std::vector<ParticleGenerator> particles;
	//Vector GravityPoint
	Vector gravityPoint;


public:
	//Konstruktor
	ParticleSystem();
	//Nachverteilung der Parikel, welche aus der Liste kommen
	void nachVerteilung(float f);
	//Setter Gravity
	void setGravity(Vector gravity);
	//Hinzufuegen von einzenlnen Paricle in die Liste
	bool addParticlesPosition(int num, Vector position);
	//Loescht Parikel
	bool deleteParticles(int i);
	//Draw Funktion
	void draw(Camera& cam);
	//calc3DRay aus Matrix.cpp (angepasst)
	Vector calc3DRay(Vector particlePosition, Camera& cam);
	//Destructor
	~ParticleSystem(void);
};
#endif //PARTICLE_SYSTEM_H