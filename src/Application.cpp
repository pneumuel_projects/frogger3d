//
//  Application.cpp
//  ogl4
//
//  Created by Philipp Lensing on 16.09.16.
//  Copyright © 2016 Philipp Lensing. All rights reserved.
//

#include "Application.h"
#ifdef WIN32
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#define _USE_MATH_DEFINES
#elif __linux__
#define GLFW_INCLUDE_GLCOREARB
#define GLFW_INCLUDE_GLEXT
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#else
#define GLFW_INCLUDE_GLCOREARB
#define GLFW_INCLUDE_GLEXT
#include <glfw/glfw3.h>
#endif

#include "LinePlaneModel.h"
#include "TrianglePlaneModel.h"
#include "Model.h"
#include "ShaderLightMapper.h"



#ifdef WIN32
#define ASSET_DIRECTORY "../../assets/"
#else
#define ASSET_DIRECTORY "../assets/"
#endif


Application::Application(GLFWwindow* pWin) : Cam(pWin), pModel(nullptr), pWindow(pWin), ShadowGenerator(2048, 2048)
{
	//Partikelsystem Fehlerhaft

	////Particle Options
	particleSystem =  ParticleSystem();
	float ww_ratio = float(100) / 1024;
	float wh_ratio = float(100) / 800;
	particleSystem.setGravity(Vector((2 * ww_ratio - 1)*LAENGE, (1 - 2 * wh_ratio)*LAENGE, 0));
	//particleSystem.addParticles(1000);
	particleSystem.addParticlesPosition(3000,Vector(10,20,0));

	//Audio
	sound.changeMusik(sound.getCurrentMusic());

	//Level Options
	levelBlockCount = 0;
	lastIndexRepositioned = 0;
	levelBlocks = nullptr;
	gameRunning = false;
	
	//Spiel laden
	createScene();


}
void Application::start()
{
    glEnable (GL_DEPTH_TEST); // enable depth-testing
    glDepthFunc (GL_LESS); // depth-testing interprets a smaller value as "closer"
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Application::update(float dtime)
{
	//Particlesystem Fehlerhaft

	//Nachverteilung der Particle
	//particleSystem.nachVerteilung(1);


	//Neupositionierung der Blöcke, falls nötig
	for (int i = 0; i < levelBlockCount; ++i) {
		// (i + levelBlockCount - 1) % levelBlockCount = Der Index des Blockes hinter dem aktuellen Block. 
		//Offset auf die Boundingbox, damit das verschieben verzögert wird und nicht so sichtbar ist.
		//Prüfen ob der Frosch in dem Block ist, und dieser nicht gerade erst verschoben wurde.
		if (lastIndexRepositioned != i && c.checkCollision(levelBlocks[i]->transform().translation() + Vector(0, 0, levelBlocks[i]->getBoundingBox().size().Z * 0.5f + 19.f), levelBlocks[i]->getBoundingBox().size(), frog->transform().translation(), frog->boundingBox().size()))
		{
			//Zuletzt durchquerten Block und den Block der am weitesten weg ist holen
			const int indexLastVisitedBlock = (i + levelBlockCount - 1) % levelBlockCount;
			const int indexLastInlineBlock = (i + levelBlockCount - 2) % levelBlockCount;

			//Wenn der Block grade besucht Block vor dem am weitesten entfernten ist:
			if(levelBlocks[indexLastVisitedBlock]->transform().translation().Z < levelBlocks[indexLastInlineBlock]->transform().translation().Z)
			{
				//Neues Offset für den zu verschiebenden Block berechnen, die Schwierigkeit des am weitesten entfernten Blockes holen.
				float lastDifficulty = levelBlocks[indexLastInlineBlock]->getDifficulty();
				Vector offset = Vector(0, 0, levelBlockCount * levelBlocks[indexLastInlineBlock]->getBoundingBox().size().Z);
				
				//Den zu verschiebenden Block positionieren
				levelBlocks[indexLastVisitedBlock]->setPosition(offset);

				//Die Schwierigkeit setzen (begrenzt auf 56 Einheiten)
				if(lastDifficulty < 56.f)
				{
					lastDifficulty += 2.f;
				}
				std::cout << "Difficulty is now " << lastDifficulty;
				levelBlocks[indexLastVisitedBlock]->setDifficulty(lastDifficulty);
			}
			lastIndexRepositioned = i;
			break;
		}

	}

	//AudioBox

	//Musik aendern nach unten
	songDown = (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_S));
	if (songDown == 1) {
		sound.setOldStateDown(1);
	}
	if (songDown == 0) {
		if (sound.getOldStateDown() == 1) {

			if (sound.getCurrentMusic() > 1) {
				sound.setCurrentMusic(sound.getCurrentMusic() - 1);
			}
			else if (sound.getCurrentMusic() == 1) {
				sound.setCurrentMusic(ANZAHLSONGS);
			}
			sound.stopPlay();
			sound.changeMusik(sound.getCurrentMusic());
			sound.setOldStateDown(0);
		}
	}

	//Musik aendern nach oben
	songUp = (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_W));
	if (songUp == 1) {
		sound.setOldStateUp(1);
	}
	if (songUp == 0) {
		if (sound.getOldStateUp() == 1) {
			
			if (sound.getCurrentMusic() < ANZAHLSONGS) {
				sound.setCurrentMusic(sound.getCurrentMusic() + 1);
			}
			else if (sound.getCurrentMusic() == ANZAHLSONGS) {
				sound.setCurrentMusic(1);
			}
			sound.stopPlay();
			sound.changeMusik(sound.getCurrentMusic());
			sound.setOldStateUp(0);
		}
	}

	//Mute der Musik
	if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_M)) {
		sound.stopPlay();
	}

	//Musik Lautstaerke verringern
	if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_A)) {
		sound.decreaseVolume();
	}

	//Musik Lautstaerke erhoehen
	if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_D)) {
		sound.increaseVolume();
	}
	


	//Collision Check + Update
	for(int i = 0; i< levelBlockCount; ++i)
	{
		//Bei Kollision: Frosch töten und Spiel anhalten
		if (levelBlocks[i]->checkCollision(frog->transform().translation(), frog->boundingBox().size()))
		{
			frog->kill();
			Cam.setSmoothMotionFactor(0.05f);
			gameRunning = false;
			Matrix pos;
			pos.translation(frog->transform().translation() + Vector(-restartButton->boundingBox().size().X - 0.5f, 0, 0));
			restartButton->transform(pos);
			Matrix rotM;
			rotM.rotationY(3.141593f);
			restartButton->transform(pos * rotM);
		}

		//Spielblöcke aktualisieren
		levelBlocks[i]->update(dtime);
	}

	//Lichter den Frosch folgen lassen
	for (auto i : ShaderLightMapper::instance().lights())
	{
		Vector currentPosition = i->position();
		i->position(currentPosition + Vector(0, 0,frog->transform().translation().Z - lastFrogPosition.Z));
	}
	lastFrogPosition = frog->transform().translation();

	//Skybox mit Frosch verschieben
	Matrix m;
	m.translation(Vector(0, 0, frog->transform().translation().Z));
	skybox->transform(m);

	//Sprung
	const float distance = 3.643f;
	const float levelWidth = 20.f;
	const float frogPosX = frog->transform().translation().X;

	//Sprung ist nur möglich, wenn das Spiel läuft und der Frosch läuft
	if (frog->isAlive() && gameRunning) {
		if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_UP))
		{
			frog->jump(Frog::FORWARD, distance);
		}
		else if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_LEFT) && frogPosX + distance < levelWidth)
		{
			frog->jump(Frog::LEFT, distance);
		}
		else if (GLFW_PRESS == glfwGetKey(pWindow, GLFW_KEY_RIGHT) && frogPosX > distance + 1.f)
		{
			frog->jump(Frog::RIGHT, distance);
		}
	}
	frog->update(dtime);

	//Kamera positionieren
	if (!frog->isAlive())
	{
	
		Cam.setPosition(frog->transform().translation() + Vector(0, 10.f, -2.f));
		
		//Leider zu langsam: Den Nebel langsam dichter werden lassen.
		/*Vector currentDensity;
		for (ModelList::iterator it = Models.begin(); it != Models.end(); ++it)
		{
		currentDensity = ((PhongShader)((*it)->shader())).fogDensity();
		currentDensity = Vector((10 - currentDensity.X) * 0.2, (20 - currentDensity.Y) * 0.2,0);
		BaseShader* bs = (*it)->shader();
		if (bs != nullptr) {
		dynamic_cast<PhongShader*>(bs)->fogDensity(currentDensity.X, currentDensity.Y);
		}
		}*/
	}
	else
	{
		Cam.setPosition(Vector(20, 5, frog->transform().translation().Z - 10));
	}

	//Kamera aktualisieren
	Cam.setTarget(frog->transform().translation());
	Cam.update();

	//Spielsteuerung
	if(!gameRunning )
	{
		//Mausposition in Weltkoordinaten umrechnen
		double newX, newY;
		glfwGetCursorPos(pWindow, &newX, &newY);
		auto camPos = this->Cam.position();
		const auto dir = calc3DRay(newX, newY, camPos);

		Vector normal(0, 1, 0);

		const auto D = normal.dot(Vector(1, 0, 0));
		const auto s = (D - normal.dot(camPos)) / (normal.dot(dir));

		const Vector pos = camPos + dir * s;

		//Spielstart (Frosch lebt und das Spiel läuft noch nicht)
		if (frog->isAlive()) {

			//Prüfen ob der Mauszeiger im Startknopf ist
			if (c.checkCollision(startButton->transform().translation(), startButton->boundingBox().size(), pos, Vector(0.5f, 0.5f, 0.5f)))
			{
				//Startknopf anheben
				Vector currentPos = startButton->transform().translation();
				currentPos = Vector(0, (0.5f - currentPos.Y) * 0.025f, 0);
				Matrix m;
				m.translation(currentPos);
				startButton->transform(m * startButton->transform());

				//Bei Mausklick Spiel starten, Startknopf verstecken
				if (GLFW_PRESS == glfwGetMouseButton(pWindow, 0)) {
					gameRunning = true;
					Matrix m;
					m.translation(Vector(0, -5, 0));
					startButton->transform(m * startButton->transform());
				}
			}
			else
			{
				//Startknopf wieder sinken lassen
				Vector currentPos = startButton->transform().translation();
				currentPos = Vector(0, (0 - currentPos.Y) * 0.25f, 0);
				Matrix m;
				m.translation(currentPos);
				startButton->transform(m * startButton->transform());
			}
		}else
		{
			//Prüfen ob der Mauszeiger im Restartknopf ist
			if (c.checkCollision(restartButton->transform().translation(), restartButton->boundingBox().size(), pos, Vector(0.5f, 0.5f, 0.5f)))
			{
				//Restartknopf anheben
				Vector currentPos = restartButton->transform().translation();
				currentPos = Vector(0, (0.5f - currentPos.Y) * 0.025f, 0);
				Matrix m;
				m.translation(currentPos);
				restartButton->transform(m * restartButton->transform());

				//Bei Mausklick Spiel neu starten, Restartknopf verstecken
				if (GLFW_PRESS == glfwGetMouseButton(pWindow, 0)) {
					Matrix m;
					m.translation(Vector(0, -5, 0));
					restartButton->transform(m * restartButton->transform());
					resetGame();
				}
			}
			else
			{
				//Restartknopf wieder sinken lassen
				Vector currentPos = restartButton->transform().translation();
				currentPos = Vector(0, (0 - currentPos.Y) * 0.25f, 0);
				Matrix m;
				m.translation(currentPos);
				restartButton->transform(m * restartButton->transform());
			}
		}
	}
}

void Application::draw()
{
	ShadowGenerator.generate(Models);

    // 1. clear screen
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ShaderLightMapper::instance().activate();
    // 2. setup shaders and draw models

	particleSystem.draw(Cam);
	
    /*for( ModelList::iterator it = Models.begin(); it != Models.end(); ++it )
    {
        (*it)->draw(Cam);
    }*/

	//Das Level zeichnen
	for (int i = 0; i< levelBlockCount; ++i)
	{
		levelBlocks[i]->draw(Cam);
	}

	//Eigene Modelle zeichnen
	startStrip->draw(Cam);
	frog->draw(Cam);
	skybox->draw(Cam);

	//Buttons nur wenn benötigt zeichnen
	if (!gameRunning) {
		if (!frog->isAlive())
		{
			restartButton->draw(Cam);
		}else
		{
			startButton->draw(Cam);
		}
	}

	ShaderLightMapper::instance().deactivate();
	
    // 3. check once per frame for opengl errors
    GLenum Error = glGetError();
    assert(Error==0);
}
void Application::end()
{
    for( ModelList::iterator it = Models.begin(); it != Models.end(); ++it )
        delete *it;
    
    Models.clear();
	
	for (int i = 0; i< levelBlockCount; ++i)
	{
		delete levelBlocks[i];
	}
	delete levelBlocks;
	delete frog;
	delete skybox;
	delete startStrip;
	delete startButton;
	delete restartButton;
}

void Application::createScene()
{
	std::cout << "Loading Level...";

	//Grünstreifen vor dem Level
	startStrip = new Model(ASSET_DIRECTORY "startblock.dae", false);
	startStrip->shader(new PhongShader(), true);
	Matrix m;
	m.translation(Vector(startStrip->boundingBox().size().X * 0.5f, 0, -startStrip->boundingBox().size().Y - 1.7861f));
	startStrip->transform(m);

	//Startknopf
	startButton = new Model(ASSET_DIRECTORY "startButton.dae", false);
	startButton->shader(new PhongShader(), true);
	m.translation(Vector(15, 0, -7));
	Matrix rotM;
	rotM.rotationY(3.141593f);
	startButton->transform(m * rotM);

	//Neustart Knopf
	restartButton = new Model(ASSET_DIRECTORY "restartButton.dae", false);
	restartButton->shader(new PhongShader(), true);

	//Level initialisieren
	const int initialBlocks = 3;
	lastIndexRepositioned = initialBlocks - 1;
	levelBlocks = new LevelBlock*[initialBlocks];
	Vector blockOffset(0, 0, 0);
	float difficulty = 10.f;
	for(int i = 0; i< initialBlocks; ++i)
	{
		levelBlocks[i] = new LevelBlock(ASSET_DIRECTORY "blocks/highway/", difficulty);
		levelBlocks[i]->setPosition(blockOffset);
		difficulty += 2.f;
		Vector tmp = Vector(0,0,levelBlocks[i]->getBoundingBox().size().Z);
		blockOffset += tmp;
		std::cout << 100/initialBlocks*(i+1) << "%";
		//levelBlocks[i]->getModels(Models);
	}
	levelBlockCount = initialBlocks;
	
	//Frosch
	frog = new Frog();
	m.translation(10.f, 0, -1.7861f * 2);
	frog->transform(m);
	lastFrogPosition = frog->transform().translation();

	//Skybox
	pModel = new Model(ASSET_DIRECTORY "skybox.obj", false);
	PhongShader* p = new PhongShader();
	p->fogDensity(300, 500);
	pModel->shader(p, true);
	pModel->shadowCaster(false);
	//Models.push_back(pModel);
	skybox = pModel;

	//Beleuchtung
	Color c = Color(1.f, 0.9f, 0.7f);
	float innerradius = 25;
	float outerradius = 90;

	DirectionalLight* dl = new DirectionalLight();
	dl->direction(Vector(1, -1, 1));
	dl->color(c);
	dl->castShadows(true);
	ShaderLightMapper::instance().addLight(dl);

	Vector a = Vector(0.5, 0.01, 0.01f);
	c = Color(0.9f, 0.7f, 0.7f);
	PointLight* pl = new PointLight();
	pl->position(Vector(15, 10, 5));
	pl->color(c);
	pl->attenuation(a);
	ShaderLightMapper::instance().addLight(pl);

	SpotLight* sl = new SpotLight();
	sl->position(Vector(10, 15, 30));
	sl->color(c);
	sl->direction(Vector(0, -0.2, -1));
	sl->innerRadius(innerradius);
	sl->outerRadius(outerradius);
	ShaderLightMapper::instance().addLight(sl);
}

void Application::resetGame()
{
	//Spiel auf Angangsstatus zurücksetzen
	lastIndexRepositioned = levelBlockCount - 1;
	Vector blockOffset(0, 0, 0);
	float difficulty = 10.f;

	//Spielblöcke neu positionieren
	for (int i = 0; i< levelBlockCount; ++i)
	{
		levelBlocks[i]->setPosition(blockOffset - levelBlocks[i]->transform().translation());
		levelBlocks[i]->setDifficulty(difficulty);
		difficulty += 2.f;
		Vector tmp = Vector(0, 0, levelBlocks[i]->getBoundingBox().size().Z);
		blockOffset += tmp;
		std::cout << 100 / levelBlockCount * (i + 1) << "%";
	}

	//Frosch zurücksetzen
	Matrix m;
	m.translation(10.f, 0, -1.7861f * 2);
	frog->transform(m);

	///Kamera zurücksetzen
	Cam.setPosition(Vector(20, 5, frog->transform().translation().Z - 10));
	Cam.setTarget(frog->transform().translation());
	Cam.setSmoothMotionFactor(1);
	Cam.update();
	Cam.setSmoothMotionFactor(0.25f);

	//Frosch wiederbeleben und Spiel starten
	frog->revive();
	gameRunning = true;
}

Vector Application::calc3DRay(float x, float y, Vector& Pos)
{
	int width, height;
	glfwGetWindowSize(this->pWindow, &width, &height);
	auto const newX = ((x * 2) / width) - 1;
	auto const newY = (((y * 2) / height) - 1) * (-1);

	auto proj = Cam.getProjectionMatrix();
	proj.invert();

	const auto dir = proj * Vector(newX, newY, 0);
	auto view = Cam.getViewMatrix();
	view.invert();

	const auto newDir = view.transformVec3x3(dir);

	return newDir;
}
