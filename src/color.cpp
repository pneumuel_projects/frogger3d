#include "color.h"

Color::Color() {
	R = 0;
	G = 0;
	B = 0;
}

Color::Color(float r, float g, float b) {
	R = r;
	G = g;
	B = b;
}

Color Color::operator*(const Color &c) const {
	return { c.R * R, c.G * G, c.B * B };
}

Color Color::operator*(const float Factor) const {
	return operator*(Color(Factor, Factor, Factor));
}

Color Color::operator+(const Color &c) const {
	return { c.R + R, c.G + G, c.B + B };
}

Color &Color::operator+=(const Color &c) {
	R += c.R;
	G += c.G;
	B += c.B;
	return *this; // dummy (remove)
}
