﻿#include "DangerObject.h"
#include "Collision.h"

#ifdef WIN32
#define ASSET_DIRECTORY "../../assets/"
#else
#define ASSET_DIRECTORY "../assets/"
#endif

DangerObject::DangerObject(): tireFrontRight(nullptr), tireFrontLeft(nullptr), tireBackRight(nullptr),
                              tireBackLeft(nullptr), leftCarLight(nullptr), rightCarLight(nullptr)
{
	speed = 0.f;
}

DangerObject::DangerObject(const char* modelPath, Vector position, float speed)
{
	std::cout << "\t\t\t|| DangerObject" << std::endl;

	//Reifen laden
	std::string tirePath = ASSET_DIRECTORY "car_tire.dae";

	tireBackLeft = new Model(tirePath.c_str());
	tireBackLeft->shader(new PhongShader(), true);

	tireFrontLeft = new Model(tirePath.c_str());
	tireFrontLeft->shader(new PhongShader(), true);

	tireBackRight = new Model(tirePath.c_str());
	tireBackRight->shader(new PhongShader(), true);

	tireFrontRight = new Model(tirePath.c_str());
	tireFrontRight->shader(new PhongShader(), true);

	// Scheinwerfer laden (Nicht aktiviert, da nur 14 Lichter unterstützt werden, das Erhöhen des Limits hat nicht funktioniert)
	const float innerradius = 45;
	const float outerradius = 60;
	Color c = Color(1.0f, 0.7f, 1.0f);
	const Vector direction = Vector(1, -1.5, 0);
	Vector a = Vector(0.5f, 0, 0.1f);
	leftCarLight = new SpotLight();
	leftCarLight->position(Vector(0, 0, 0));
	leftCarLight->color(c);
	leftCarLight->direction(direction);
	leftCarLight->innerRadius(innerradius);
	leftCarLight->outerRadius(outerradius);
	leftCarLight->castShadows(true);
	leftCarLight->attenuation(a);
	// ShaderLightMapper::instance().addLight(leftCarLight);

	rightCarLight = new SpotLight();
	rightCarLight->position(Vector(0, 0, 0));
	rightCarLight->color(c);
	rightCarLight->direction(direction);
	rightCarLight->innerRadius(innerradius);
	rightCarLight->outerRadius(outerradius);
	rightCarLight->castShadows(true);
	leftCarLight->attenuation(a);
	// ShaderLightMapper::instance().addLight(rightCarLight);

	//Model laden und Parameter übernehmen
	if (!this->load(modelPath, false))
	{
		throw std::exception("DangerObject: Path Constructor: Model konnte nicht geladen werden!");
	}
	this->speed = speed;
	shadowCaster(true);
	this->Transform = (Matrix()).translation(position);
	setPosition(position);
	this->speed = speed;
	this->shader(new PhongShader(), true);
}

DangerObject::~DangerObject() {
	delete(tireBackLeft);
	delete(tireBackRight);
	delete(tireFrontLeft);
	delete(tireFrontRight);
	delete leftCarLight;
	delete rightCarLight;
}

bool DangerObject::checkCollision(Vector position, Vector boundingBoxSize) const
{
	Collision c;
	return c.checkCollision(Transform.translation() + Vector(1.f,0,0), BoundingBox.size(), position, boundingBoxSize);
}

void DangerObject::update(float deltaTime)
{
	//Je nach Geschwindigkeit bewegen
	Vector currentTranslation = Transform.translation();
	float translationOffset = speed * deltaTime;
	currentTranslation.X += translationOffset;
	this->Transform.translation(currentTranslation);

	//Reifen positionieren
	//1.570796f = 90°
	Matrix rightSideRotation = Matrix();
	rightSideRotation.rotationX(-1.570796f);

	Matrix leftSideRotation = Matrix();
	leftSideRotation.rotationX(1.570796f);

	Matrix wheelDriveRotation = Matrix();
	wheelDriveRotation.rotationZ(-Transform.translation().X * 1.5f);

	//Reifenposition aktualisieren
	Matrix translationMatrix = Matrix();
	translationMatrix.translation(currentTranslation + Vector(-0.809041f, 0.255806f, 0.846513f));
	tireBackLeft->transform(translationMatrix * wheelDriveRotation * leftSideRotation);
	translationMatrix.translation(currentTranslation + Vector(-0.809041f, 0.255806f, -0.846513f));
	tireBackRight->transform(translationMatrix *  wheelDriveRotation * rightSideRotation);
	translationMatrix.translation(currentTranslation + Vector(1.42968f, 0.255806f, 0.846513f));
	tireFrontLeft->transform(translationMatrix *  wheelDriveRotation * leftSideRotation);
	translationMatrix.translation(currentTranslation + Vector(1.42968f, 0.255806f, -0.846513f));
	tireFrontRight->transform(translationMatrix *  wheelDriveRotation * rightSideRotation);

	//Lichtposition aktivieren
	leftCarLight->position(this->Transform.translation() + Vector(3.f, 1.8f, -0.7f));
	rightCarLight->position(this->Transform.translation() + Vector(3.f, 1.8f, 0.7f));
}

void DangerObject::draw(const BaseCamera & Cam) 
{
	tireBackLeft->draw(Cam);
	tireFrontLeft->draw(Cam);
	tireBackRight->draw(Cam);
	tireFrontRight->draw(Cam);

	Model::draw(Cam);
}

void DangerObject::setPosition(Vector position)
{
	Matrix m;
	m.translation(position);
	Transform = Transform * m;
}

void DangerObject::setSpeed(float speed)
{
	this->speed = speed;
}

void DangerObject::getModels(std::list<BaseModel*>& list)
{
	list.push_back(this);
	list.push_back(tireFrontLeft);
	list.push_back(tireFrontRight);
	list.push_back(tireBackRight);
	list.push_back(tireBackLeft);
}

