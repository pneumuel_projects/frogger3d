
#ifndef LevelBlock_h
#define LevelBlock_h

#include "BaseModel.h"
#include "DangerStrip.h"
#include <filesystem>
#include "Strip.h"
#include "Collision.h"

class Strip;

class LevelBlock : public BaseModel
{
public:
	LevelBlock();
	LevelBlock(std::string blockDirectory, float difficulty = 0);

	~LevelBlock() override;
	virtual void draw(const BaseCamera& Cam);
	virtual bool checkCollision(Vector position, Vector boundingBox);
	virtual AABB getBoundingBox();
	virtual void setPosition(Vector& position);
	virtual void update(float deltaTime);
	void getModels(std::list<BaseModel*>& list);
	void setDifficulty(float difficulty);
	float getDifficulty() const { return difficulty; }
protected:

private:
	float difficulty;
	DangerStrip** dangerStrips;
	int dangerStripCount;
	Strip** strips;
	int stripCount;
	Model* facade;
	Vector position;
	AABB boundingBox;
};

#endif /* LevelBlock */
