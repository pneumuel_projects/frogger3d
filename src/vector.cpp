#include "vector.h"
#include <cmath>

Vector::Vector(float x, float y, float z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector::Vector(Vector vec, float maginitude) {
	vec.normalize();
	/*if (maginitude == 0)
		throw "invalid vector";*/
	X = maginitude * vec.X;
	Y = maginitude * vec.Y;
	Z = maginitude * vec.Z;
}

Vector::Vector()
= default;

float Vector::dot(const Vector& v) const
{
	return X * v.X + Y * v.Y + Z * v.Z;
}

Vector Vector::cross(const Vector& v) const
{
	Vector result;
	result.X = Y * v.Z - Z * v.Y;
	result.Y = Z * v.X - X * v.Z;
	result.Z = X * v.Y - Y * v.X;
	return result;
}

Vector Vector::operator/(float val) const {

	return Vector(X / val, Y / val, Z / val);
}

Vector Vector::operator+(const Vector& v) const
{
	return { X + v.X, Y + v.Y, Z + v.Z };
}

Vector Vector::operator-(const Vector& v) const
{
	return { X - v.X, Y - v.Y, Z - v.Z };
}

Vector Vector::operator*(float c) const
{
	return { X * c, Y * c, Z * c };
}

Vector Vector::operator-() const
{
	return { 0 - X, 0 - Y, 0 - Z };
}

Vector Vector::unit() const {
	float m = magnitude();
	/*if (m == 0)
		throw "invalid vector";*/
	return Vector(X / m, Y / m, Z / m);
}

Vector& Vector::operator+=(const Vector& v)
{
	X += v.X;
	Y += v.Y;
	Z += v.Z;
	return *this;
}

Vector& Vector::normalize()
{
	float len = length();
	X = X / len;
	Y = Y / len;
	Z = Z / len;
	return *this;
}

float Vector::length() const
{
	return std::sqrt(lengthSquared());
}

float Vector::lengthSquared() const
{
	return X * X + Y * Y + Z * Z;
}

Vector Vector::reflection(const Vector& normal) const
{
	//Vnew = -2*(V dot N)*N + V
	return normal * (-2 * dot(normal)) + *this;
}

float Vector::triangleS(const Vector &d, const Vector &a, const Vector &b, const Vector &c) const
{
	//N und D berechnen
	Vector x(b - a);
	Vector y(c - a);
	Vector n(x.cross(y));
	n.normalize();
	float df = n.dot(a);

	//S berechnen
	return ((df - (n.dot(*this))) / n.dot(d));
}

bool Vector::triangleIntersectionGivenS(const Vector &d, const Vector &a, const Vector &b, const Vector &c, float s) const
{
	//P berechnen
	Vector p = *this + d * s;

	float abc = flaecheDreieck(a, b, c);
	float abp = flaecheDreieck(a, b, p);
	float acp = flaecheDreieck(a, c, p);
	float bcp = flaecheDreieck(b, c, p);

	return abc + 1e-6f >= abp + acp + bcp && s >= 0;
}

bool Vector::triangleIntersection(const Vector& d, const Vector& a, const Vector& b, const Vector& c, float& s) const
{
	//N und D berechnen
	Vector x(b - a);
	Vector y(c - a);
	Vector n(x.cross(y));
	n.normalize();
	float df = n.dot(a);

	//S berechnen
	s = (df - (n.dot(*this))) / n.dot(d);

	//P berechnen
	Vector p = *this + d * s;

	float abc = flaecheDreieck(a, b, c);
	float abp = flaecheDreieck(a, b, p);
	float acp = flaecheDreieck(a, c, p);
	float bcp = flaecheDreieck(b, c, p);

	return abc + 1e-6f >= abp + acp + bcp && s >= 0;
}

float Vector::flaecheDreieck(const Vector& a, const Vector& b, const Vector& c) const {
	Vector tmp = (b - a).cross((c - a));
	return tmp.length() / 2.0f;
}

/*Quelltext uebernommen github.com/usmanshaahid/ParticleSystem*/
void Vector::rotate(const Vector& vec, double angle)
{
	double rotationMatrix[4][4];

	double L = (vec.X*vec.X + vec.Y * vec.Y + vec.Z * vec.Z);
	double u2 = vec.X * vec.X;
	double v2 = vec.Y * vec.Y;
	double w2 = vec.Z * vec.Z;

	rotationMatrix[0][0] = (u2 + (v2 + w2) * cos(angle)) / L;
	rotationMatrix[0][1] = (vec.X * vec.Y * (1 - cos(angle)) - vec.Z * sqrt(L) * sin(angle)) / L;
	rotationMatrix[0][2] = (vec.X * vec.Z * (1 - cos(angle)) + vec.Y * sqrt(L) * sin(angle)) / L;
	rotationMatrix[0][3] = 0.0;

	rotationMatrix[1][0] = (vec.X * vec.Y * (1 - cos(angle)) + vec.Z * sqrt(L) * sin(angle)) / L;
	rotationMatrix[1][1] = (v2 + (u2 + w2) * cos(angle)) / L;
	rotationMatrix[1][2] = (vec.Y * vec.Z * (1 - cos(angle)) - vec.X * sqrt(L) * sin(angle)) / L;
	rotationMatrix[1][3] = 0.0;

	rotationMatrix[2][0] = (vec.X * vec.Z * (1 - cos(angle)) - vec.Y * sqrt(L) * sin(angle)) / L;
	rotationMatrix[2][1] = (vec.Y * vec.Z * (1 - cos(angle)) + vec.X * sqrt(L) * sin(angle)) / L;
	rotationMatrix[2][2] = (w2 + (u2 + v2) * cos(angle)) / L;
	rotationMatrix[2][3] = 0.0;

	rotationMatrix[3][0] = 0.0;
	rotationMatrix[3][1] = 0.0;
	rotationMatrix[3][2] = 0.0;
	rotationMatrix[3][3] = 1.0;

	double inputMatrix[4][1] = { 0.0, 0.0, 0.0 };
	double outputMatrix[4][1] = { 0.0, 0.0, 0.0, 0.0 };

	inputMatrix[0][0] = X;
	inputMatrix[1][0] = Y;
	inputMatrix[2][0] = Z;
	inputMatrix[3][0] = 1.0;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 1; j++) {
			outputMatrix[i][j] = 0;
			for (int k = 0; k < 4; k++) {
				outputMatrix[i][j] += rotationMatrix[i][k] * inputMatrix[k][j];
			}
		}
	}

	X = (float)outputMatrix[0][0];
	Y = (float)outputMatrix[1][0];
	Z = (float)outputMatrix[2][0];

}

float Vector::magnitude() const
{
	return sqrt(X*X + Y * Y + Z * Z);
}
