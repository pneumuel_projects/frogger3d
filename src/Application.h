//
//  Application.hpp
//  ogl4
//
//  Created by Philipp Lensing on 16.09.16.
//  Copyright © 2016 Philipp Lensing. All rights reserved.
//

#ifndef Application_hpp
#define Application_hpp

#include <list>
#include "Camera.h"
#include "PhongShader.h"
#include "BaseModel.h"
#include "ShadowMapGenerator.h"
#include "LevelBlock.h"
#include "Frog.h"
#include "ParticleSystem.h"
#include "Sound.h"
#include "Collision.h"

class Application
{
public:
    typedef std::list<BaseModel*> ModelList;
    Application(GLFWwindow* pWin);
    void start();
    void update(float dtime);
    void draw();
    void end();
protected:
	void createScene();
	void resetGame();
	Vector calc3DRay(float x, float y, Vector& Pos);
    
	Camera Cam;
    
	ModelList Models;
	Frog* frog{};	
	LevelBlock** levelBlocks;
	BaseModel* pModel;
	BaseModel* skybox{};
	Model* startStrip{};
	Model* startButton{};
	Model* restartButton{};

    GLFWwindow* pWindow;
	int levelBlockCount;
	ShadowMapGenerator ShadowGenerator;
	
	Collision c;
	
	//Particlesystem Fehlerhaft
	ParticleSystem particleSystem;
	
	//Soundsystem
	//Musik
	Sound sound;
	int songDown = 0;
	int songUp = 0;

	int lastIndexRepositioned;
	Vector lastFrogPosition;
	bool gameRunning;
};

#endif /* Application_hpp */