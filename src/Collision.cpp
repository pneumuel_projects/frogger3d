#include "Collision.h"

bool Collision::checkCollision(Vector aPosition, Vector aBoundingBoxSize, Vector bPosition, Vector bBoundingBoxSize)
{
	//Einfache, boundingbox-basierte Collision detection
	//Quelle: https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_collision_detection

	//Die Maxima und Minima der Boundingbox anpassen, da die Position in die Mitte des Objektes zeigt.
	Vector aBoundingBoxCenter(aBoundingBoxSize.X / 2, aBoundingBoxSize.Y / 2, aBoundingBoxSize.Z / 2);
	Vector bBoundingBoxCenter(bBoundingBoxSize.X / 2, bBoundingBoxSize.Y / 2, bBoundingBoxSize.Z / 2);
	Vector aMin(aPosition - aBoundingBoxCenter);
	Vector aMax(aPosition + aBoundingBoxCenter);
	Vector bMin(bPosition - bBoundingBoxCenter);
	Vector bMax(bPosition + bBoundingBoxCenter);

	return (aMin.X <= bMax.X && aMax.X >= bMin.X) &&
		(aMin.Y <= bMax.Y && aMax.Y >= bMin.Y) &&
		(aMin.Z <= bMax.Z && aMax.Z >= bMin.Z);
}