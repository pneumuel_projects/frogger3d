#include "Strip.h"
#include "Collision.h"


Strip::Strip()
= default;

Strip::Strip(const char* blockPath, Vector position)
{
	std::cout << "\t\t|| Grass Strip" << std::endl;
	
	//Model laden und positionieren
	this->path = blockPath;
	std::string laneModelPath = blockPath;
	laneModelPath = laneModelPath.append("grass_lane.dae");
	if(!load(laneModelPath.c_str(), false))
	{
		throw std::exception();
	}
	Transform.translation(position + Vector(BoundingBox.size().X / 2, 0, 0));
	BaseModel::shader(new PhongShader(), true);
	shadowCaster(true);

	//F�r bessere Kollisionserkennung die H�he und Breite der Boundingbox erh�hen.
	Vector bboXMax = BoundingBox.Max;
	bboXMax = Vector(300, 100, bboXMax.Z);
	BoundingBox.Max = bboXMax;
}

Strip::~Strip()
= default;

bool Strip::checkCollision(Vector position, Vector boundingBoxSize) const
{
	Collision c;
	return c.checkCollision(this->Transform.translation(), BoundingBox.size(), position, boundingBoxSize);
}

void Strip::update(float deltaTime)
{
}

void Strip::setPosition(Vector position)
{
	Matrix m;
	m.translation(position);
	Transform = Transform * m;
}

void Strip::getModels(std::list<BaseModel*>& list)
{
	list.push_back(this);
}
