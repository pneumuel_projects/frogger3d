#include "ParticleGenerator.h"


/*Inspiration von github.com/usmanshaahid/ParticleSystem
wurde aber stark abgeandert*/


//Hilffunktion Random Float
float ParticleGenerator::randFlow()
{
	float value = rand() / float(RAND_MAX);
	return value;
}

//Konstruktor
ParticleGenerator::ParticleGenerator(Vector position) 
{
	mass = rand() % (MAXMASS - MINMASS) + MINMASS;
	velocity = Vector(Vector(randFlow(), randFlow(), randFlow()), MINVELOCITYINIT + rand() % (MAXVELOCITYINIT - MINVELOCITYINIT));
	this->position = position;
}

//Konstruktor COPY
ParticleGenerator::ParticleGenerator(const ParticleGenerator &other) {
	this->mass = other.mass;
	this->velocity = other.velocity;
	this->position = other.position;
	this->initialPosition = other.initialPosition;
}

//Konstruktor
ParticleGenerator::ParticleGenerator()
{
	this->mass = rand() % (MAXMASS - MINMASS) + MINMASS;
	this->velocity = Vector(Vector(randFlow(), randFlow(), randFlow()), MINVELOCITYINIT + rand() % (MAXVELOCITYINIT - MINVELOCITYINIT));
	this->position = Vector((1 - 2 * randFlow())*LAENGE, (1 - 2 * randFlow())*LAENGE, (1 - 2 * randFlow())*LAENGE);
	this->initialPosition = position;
}

//Random Verteilung der Parikel
void ParticleGenerator::verteilung(float t, Vector force)
{
	
	Vector maechtigkeit = force / mass;

	velocity = velocity + maechtigkeit * (t / 50000.0);

	if (velocity.magnitude() >= MAXVELOCITY)
		velocity = Vector(velocity.unit(), MAXVELOCITY);

	position = position + velocity * (t / 700.0);
}

//Destruktor
ParticleGenerator::~ParticleGenerator(void) {
}

//Getter Position
Vector ParticleGenerator::getPosistion()
{
	return position;
}
