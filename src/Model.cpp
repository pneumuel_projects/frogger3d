//
//  Model.cpp
//  ogl4
//
//  Created by Philipp Lensing on 21.09.16.
//  Copyright © 2016 Philipp Lensing. All rights reserved.
//

#include "Model.h"
#include "PhongShader.h"
#include <list>

#ifdef WIN32
#define ASSET_DIRECTORY "../../assets/"
#else
#define ASSET_DIRECTORY "../assets/"
#endif

#define SCALE 5.f
Model::Model() : pMeshes(NULL), MeshCount(0), pMaterials(NULL), MaterialCount(0)
{

}
Model::Model(const char* ModelFile, bool FitSize) : pMeshes(NULL), MeshCount(0), pMaterials(NULL), MaterialCount(0)
{
	bool ret = load(ModelFile);
	if (!ret)
		throw std::exception();
}
Model::~Model()
{
	//Ressourcen aus loadMaterials freigeben
	for (int i = 0; i < this->MaterialCount; i++) {
		//delete this->pMaterials[i].DiffTex;
	}
	delete[] this->pMaterials;

	delete[] this->pMeshes;

	deleteNodes(&RootNode);
}

void Model::deleteNodes(Node* pNode)
{
	if (!pNode)
		return;
	for (unsigned int i = 0; i < pNode->ChildCount; ++i)
		deleteNodes(&(pNode->Children[i]));
	if (pNode->ChildCount > 0)
		delete[] pNode->Children;
	if (pNode->MeshCount > 0)
		delete[] pNode->Meshes;
}

bool Model::load(const char* ModelFile, bool FitSize)
{
	const aiScene* pScene = aiImportFile(ModelFile, aiProcessPreset_TargetRealtime_Fast | aiProcess_TransformUVCoords | aiProcess_FlipUVs);

	if (pScene == NULL || pScene->mNumMeshes <= 0)
		return false;

	Filepath = ModelFile;
	Path = Filepath;
	size_t pos = Filepath.rfind('/');
	if (pos == std::string::npos)
		pos = Filepath.rfind('\\');
	if (pos != std::string::npos)
		Path.resize(pos + 1);

	loadMeshes(pScene, FitSize);
	loadMaterials(pScene);
	loadNodes(pScene);

	return true;
}

void Model::loadMeshes(const aiScene* pScene, bool FitSize)
{
	this->calcBoundingBox(pScene, this->BoundingBox);

	this->MeshCount = pScene->mNumMeshes;
	this->pMeshes = new Mesh[this->MeshCount];

	Vector tmp;

	for (int meshIndex = 0; meshIndex < this->MeshCount; meshIndex++) {

		//Aktuelles Mesh laden und Indexliste übertragen
		Mesh& currentMesh = this->pMeshes[meshIndex];
		currentMesh.MaterialIdx = pScene->mMeshes[meshIndex]->mMaterialIndex;

		//Vertices übertragen
		currentMesh.VB.begin();
		for (int vertexIndex = 0; vertexIndex < pScene->mMeshes[meshIndex]->mNumVertices; ++vertexIndex) {

			//Vertexposition übertragen und skalieren
			tmp.X = pScene->mMeshes[meshIndex]->mVertices[vertexIndex].x;
			tmp.Y = pScene->mMeshes[meshIndex]->mVertices[vertexIndex].y;
			tmp.Z = pScene->mMeshes[meshIndex]->mVertices[vertexIndex].z;

			//Normale übertragen
			currentMesh.VB.addNormal(pScene->mMeshes[meshIndex]->mNormals[vertexIndex].x, pScene->mMeshes[meshIndex]->mNormals[vertexIndex].y, pScene->mMeshes[meshIndex]->mNormals[vertexIndex].z);

			//Prüfen ob Textur existiert
			if (pScene->mMeshes[meshIndex]->mTextureCoords[0]) {
				//Texturkoordinaten übernehmen
				float u = pScene->mMeshes[meshIndex]->mTextureCoords[0][vertexIndex].x;
				float v = pScene->mMeshes[meshIndex]->mTextureCoords[0][vertexIndex].y;
				currentMesh.VB.addTexcoord0(u, v);
			}

			//Vertex hinzufügen
			currentMesh.VB.addVertex(tmp);
		}
		currentMesh.VB.end();

		//Indices übertragen
		currentMesh.IB.begin();
		for (int faceIndex = 0; faceIndex < pScene->mMeshes[meshIndex]->mNumFaces; faceIndex++) {
			for (int indexIndex = 0; indexIndex < pScene->mMeshes[meshIndex]->mFaces[faceIndex].mNumIndices; indexIndex++) {
				currentMesh.IB.addIndex(pScene->mMeshes[meshIndex]->mFaces[faceIndex].mIndices[indexIndex]);
			}
		}
		currentMesh.IB.end();
	}


	//Skalierung durchführen
	/*if (FitSize) {


	Matrix sMat;
	sMat.scale(SCALE);
	this->transform(sMat);
	}*/
}
void Model::loadMaterials(const aiScene* pScene)
{
	this->MaterialCount = pScene->mNumMaterials;
	this->pMaterials = new Material[this->MaterialCount];

	for (int i = 0; i < this->MaterialCount; i++) {
		aiMaterial* aMat = pScene->mMaterials[i];

		//Textur laden
		aiString path;
		if (AI_SUCCESS == aMat->GetTexture(aiTextureType_DIFFUSE, 0, &path)) {
			this->pMaterials[i].DiffTex = Texture::LoadShared((this->Path + path.data).c_str());
		}

		if (AI_SUCCESS == aMat->GetTexture(aiTextureType_NORMALS, 0, &path)) {
			this->pMaterials[i].NormalTex = Texture::LoadShared((this->Path + path.data).c_str());
		}

		if (AI_SUCCESS == aMat->GetTexture(aiTextureType_NORMALS, 0, &path)) {
			this->pMaterials[i].NormalTex = Texture::LoadShared((this->Path + path.data).c_str());
		}

		//Diffusion laden
		aiColor4D color;
		if (AI_SUCCESS == aMat->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
			this->pMaterials[i].DiffColor = Color(color.r, color.g, color.b);
		}

		//Specular Color laden
		if (AI_SUCCESS == aMat->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
			this->pMaterials[i].SpecColor = Color(color.r, color.g, color.b);
		}

		//Ambient Color laden
		if (AI_SUCCESS == aMat->Get(AI_MATKEY_COLOR_AMBIENT, color)) {
			this->pMaterials[i].AmbColor = Color(color.r, color.g, color.b);
		}

		//Specular exponent laden
		float exponent;
		if (AI_SUCCESS == aMat->Get(AI_MATKEY_SHININESS, exponent)) {
			this->pMaterials[i].SpecExp = exponent;
		}


	}
}
void Model::calcBoundingBox(const aiScene* pScene, AABB& Box)
{
	Vector vMin(pScene->mMeshes[0]->mVertices[0].x, pScene->mMeshes[0]->mVertices[0].y, pScene->mMeshes[0]->mVertices[0].z);
	Vector vMax(vMin.X, vMin.Y, vMin.Z);
	for (int i = 0; i < pScene->mNumMeshes; i++) {
		for (int j = 0; j < pScene->mMeshes[i]->mNumVertices; j++) {
			vMin.X = std::fmin(pScene->mMeshes[i]->mVertices[j].x, vMin.X);
			vMin.Y = std::fmin(pScene->mMeshes[i]->mVertices[j].z, vMin.Y);
			vMin.Z = std::fmin(pScene->mMeshes[i]->mVertices[j].z, vMin.Z);

			vMax.X = std::fmax(pScene->mMeshes[i]->mVertices[j].x, vMax.X);
			vMax.Y = std::fmax(pScene->mMeshes[i]->mVertices[j].z, vMax.Y);
			vMax.Z = std::fmax(pScene->mMeshes[i]->mVertices[j].z, vMax.Z);
		}
	}
	Box.Min = vMin;
	Box.Max = vMax;
}

void Model::loadNodes(const aiScene* pScene)
{
	deleteNodes(&RootNode);
	copyNodesRecursive(pScene->mRootNode, &RootNode);
}

void Model::copyNodesRecursive(const aiNode* paiNode, Node* pNode)
{
	pNode->Name = paiNode->mName.C_Str();
	pNode->Trans = convert(paiNode->mTransformation);

	if (paiNode->mNumMeshes > 0)
	{
		pNode->MeshCount = paiNode->mNumMeshes;
		pNode->Meshes = new int[pNode->MeshCount];
		for (unsigned int i = 0; i < pNode->MeshCount; ++i)
			pNode->Meshes[i] = (int)paiNode->mMeshes[i];
	}

	if (paiNode->mNumChildren <= 0)
		return;

	pNode->ChildCount = paiNode->mNumChildren;
	pNode->Children = new Node[pNode->ChildCount];
	for (unsigned int i = 0; i < paiNode->mNumChildren; ++i)
	{
		copyNodesRecursive(paiNode->mChildren[i], &(pNode->Children[i]));
		pNode->Children[i].Parent = pNode;
	}
}

void Model::applyMaterial(unsigned int index)
{
	if (index >= MaterialCount)
		return;

	PhongShader* pPhong = dynamic_cast<PhongShader*>(shader());
	if (!pPhong) {
		std::cout << "Model::applyMaterial(): WARNING Invalid shader-type. Please apply PhongShader for rendering models.\n";
		return;
	}

	Material* pMat = &pMaterials[index];
	pPhong->ambientColor(pMat->AmbColor);
	pPhong->diffuseColor(pMat->DiffColor);
	pPhong->specularExp(pMat->SpecExp);
	pPhong->specularColor(pMat->SpecColor);
	pPhong->diffuseTexture(pMat->DiffTex);
	if (pMat->NormalTex != nullptr) {
		pPhong->normalTexture(pMat->NormalTex);
	}
}

void Model::draw(const BaseCamera& Cam)
{
	if (!pShader) {
		std::cout << "BaseModel::draw() no shader found" << std::endl;
		return;
	}
	pShader->modelTransform(transform());

	std::list<Node*> DrawNodes;
	DrawNodes.push_back(&RootNode);

	while (!DrawNodes.empty())
	{
		Node* pNode = DrawNodes.front();
		Matrix GlobalTransform;

		if (pNode->Parent != NULL)
			pNode->GlobalTrans = pNode->Parent->GlobalTrans * pNode->Trans;
		else
			pNode->GlobalTrans = transform() * pNode->Trans;

		pShader->modelTransform(pNode->GlobalTrans);

		for (unsigned int i = 0; i < pNode->MeshCount; ++i)
		{
			Mesh& mesh = pMeshes[pNode->Meshes[i]];
			mesh.VB.activate();
			mesh.IB.activate();
			applyMaterial(mesh.MaterialIdx);
			pShader->activate(Cam);
			glDrawElements(GL_TRIANGLES, mesh.IB.indexCount(), mesh.IB.indexFormat(), 0);
			mesh.IB.deactivate();
			mesh.VB.deactivate();
		}
		for (unsigned int i = 0; i < pNode->ChildCount; ++i)
			DrawNodes.push_back(&(pNode->Children[i]));

		DrawNodes.pop_front();
	}
}

Matrix Model::convert(const aiMatrix4x4& m)
{
	return Matrix(m.a1, m.a2, m.a3, m.a4,
		m.b1, m.b2, m.b3, m.b4,
		m.c1, m.c2, m.c3, m.c4,
		m.d1, m.d2, m.d3, m.d4);
}

