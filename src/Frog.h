#pragma once
#include "Model.h"
#include "LineBoxModel.h"
#include "Sound.h"

class Frog :
	public Model
{
public:
	enum Direction { FORWARD, LEFT, RIGHT };
	Frog();
	~Frog();

	virtual void update(float deltaTime);
	virtual void draw(const BaseCamera & Cam);
	void jump(Direction direction, float distance);
	bool isAlive() { return alive; }
	void kill();
	void revive();

	//Frosch Soundeffekte
	Sound frogSound;
	
private:
	bool jumping;
	float velocityX, velocityY;
	float gravity;
	Direction direction;
	bool alive;
	float totalTime{};
	Vector lastJumpPosition;
};

