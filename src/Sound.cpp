#include "Sound.h"

/*IRRKLANG LIB verwendet ambiera.com/irrklang */


/*Konstruktor f�r das Soundsystem*/
Sound::Sound() {
	SoundEngine = irrklang::createIrrKlangDevice();
	currentMusic = 1;
	oldStateDown = 0;
	oldStateUp = 0;
	deadSound = 0;
	volume = 50;
}

//aendert die Musik je nach eingabe der Variable
void Sound::changeMusik(int i) {
	switch (i) {
	case 1: SoundEngine->play2D("../../assets/audio/breakout.mp3", GL_TRUE);
		break;
	case 2: SoundEngine->play2D("../../assets/audio/HeavenHell.mp3", GL_TRUE);
		break;
	case 3: SoundEngine->play2D("../../assets/audio/LevelUp.mp3", GL_TRUE);
		break;
	default: stopPlay();
	}
}

//Lautstaerke erhoehen
void Sound::increaseVolume() {
	SoundEngine->setSoundVolume(volume++);
}

//Lautstaerke reduzieren
void Sound::decreaseVolume() {
	SoundEngine->setSoundVolume(volume--);
}

//Soundeffekt sprung
void Sound::playJump() {
	(SoundEngine->play2D("../../assets/audio/frog.mp3", GL_FALSE));
}

//Soundeffekt tot
void Sound::playDead() {
	(SoundEngine->play2D("../../assets/audio/Crash.mp3", GL_FALSE));
}


//Musik anhalten
void Sound::stopPlay() {
	SoundEngine->stopAllSounds();
}

//Setter CurrentMusic
void Sound::setCurrentMusic(const int i) {
	currentMusic = i;
}

//Getter CurrentMusic
int Sound::getCurrentMusic()const {
	return currentMusic;
}

//Setter OldStateDown
void Sound::setOldStateDown(const int i) {
	oldStateDown = i;
}

//GetterOldStateDown
int Sound::getOldStateDown()const {
	return oldStateDown;
}

//Setter OldStateUp
void Sound::setOldStateUp(const int i) {
	oldStateDown = i;
}

//Getter OldStateUp
int Sound::getOldStateUp()const {
	return oldStateDown;
}

//Setter DeadSound
void Sound::setDeadSound(const int i) {
	deadSound = i;
}

//Getter DeadSound
int Sound::getDeadSound() const {
	return deadSound;
}


