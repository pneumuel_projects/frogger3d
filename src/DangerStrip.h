
#ifndef DangerStrip_h
#define DangerStrip_h

#include "BaseModel.h"
#include "DangerObject.h"
#include "BaseStrip.h"
#include "Aabb.h"
#include <filesystem>
#include <random>

class DangerStrip : public Model
{
public:
	DangerStrip();
	DangerStrip(const char* blockPath, Vector position, float difficulty = 0);
	virtual ~DangerStrip();

	virtual bool checkCollision(Vector position, Vector boundingBoxSize) const;
	virtual void update(float deltaTime);
	virtual void draw(const BaseCamera& Cam);
	virtual void setPosition(Vector position);
	virtual AABB getBoundingBox(){ return this->boundingBox(); }
	virtual std::string& getPath() { return path; }
	virtual float getDifficulty() { return difficulty; }
	void setDifficulty(float difficulty);
	void getModels(std::list<BaseModel*>& list);
protected:

private:
	virtual float calculateSpeed();
	std::string path;
	float difficulty;
	DangerObject** dangerObjects;
	int dangerObjectCount;
	int lastDangerObjectIndex;
	float dangerObjectOffset;
};

#endif /* DangerStrip */
