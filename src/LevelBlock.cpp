#include "LevelBlock.h"

void LevelBlock::draw(const BaseCamera & Cam)
{
	//Alle Gefählichen Streifen und Grassstreifen zeichnen
	for (int i = 0; i < this->dangerStripCount; ++i)
	{
		dangerStrips[i]->draw(Cam);
	}

	for (int i = 0; i < this->stripCount; ++i)
	{
		strips[i]->draw(Cam);
	}

	//Facade zeichnen
	this->facade->draw(Cam);
}

LevelBlock::LevelBlock(): difficulty(0)
{
	facade = nullptr;
	dangerStripCount = 0;
	dangerStrips = nullptr;
	strips = nullptr;
	stripCount = 0;
}

LevelBlock::LevelBlock(std::string blockDirectory, float difficulty) {
	std::cout << "\t|| LevelBlock" << std::endl;
	
	this->difficulty = difficulty;
	position = Vector(0, 0, 0);

	//Facade laden
	std::string facadePath = blockDirectory;
    this->facade = new Model((facadePath.append("facade_left.dae")).c_str(), false);
	this->facade->shader(new PhongShader(), true);
	Matrix rotM;
	rotM.rotationY(3.141593f);
	Matrix transM;
	transM.translation(0, 0, -1.7861);
	this->facade->transform(this->facade->transform() * transM  * rotM);

    //Die Streifen laden
    int laneCount = 4;
    float blockDepth = 0;
	Vector stripOffset = Vector(0, 0, 0);

	this->dangerStrips = new DangerStrip*[laneCount];
	this->strips = new Strip*[laneCount];

	dangerStripCount = laneCount;
	stripCount = laneCount;
	const char* blockDir = blockDirectory.c_str();
	float difficultyOffset = 0.5f;

    for (int i = 0; i < laneCount; ++i)
    {
        //DangerStrip erzeugen und in Array ablegen
        DangerStrip* ds = new DangerStrip(blockDir, stripOffset, difficulty + difficultyOffset);
		Matrix transM;
		transM.translation(24, 0, 0);
		ds->transform(transM * ds->transform());
		float boundingBoxSizeZ = 3.6804f;
        blockDepth += boundingBoxSizeZ;
		stripOffset += Vector(0, 0, boundingBoxSizeZ);
		this->dangerStrips[i] = ds;
		difficultyOffset += difficultyOffset;

		//Normalen Strip generieren
		Strip* gs = new Strip(blockDir, stripOffset);
		gs->transform(transM * gs->transform());
		boundingBoxSizeZ = 3.6804f;
		blockDepth += boundingBoxSizeZ;
		stripOffset += Vector(0, 0, boundingBoxSizeZ);
		this->strips[i] = gs;
    }

	//Eigene BoundingBox setzen
	this->boundingBox = AABB(0,0,0,100, 50, blockDepth);
}

AABB LevelBlock::getBoundingBox() {
    return this->boundingBox;
}

void LevelBlock::setPosition(Vector& position)
{ 
	//Eigene Position und aller untergeordneten Objekte setzen
	Matrix m;
	m.translation(position);
	Transform = Transform * m;
	this->facade->transform(m * this->facade->transform());

	for (int i = 0; i< dangerStripCount; ++i)
	{
		dangerStrips[i]->setPosition(position);
	}

	for (int i = 0; i< stripCount; ++i)
	{
		strips[i]->setPosition(position);
	}
}

void LevelBlock::update(float deltaTime)
{
	//Alle Objekte durchlaufen und aktualisieren
	for (int i = 0; i< dangerStripCount; ++i)
	{
		dangerStrips[i]->update(deltaTime);
	}

	for (int i = 0; i< stripCount; ++i)
	{
		strips[i]->update(deltaTime);
	}
}

void LevelBlock::getModels(std::list<BaseModel*>& list)
{
	list.push_back(this);
	list.push_back(facade);
	for (int i = 0; i< dangerStripCount; ++i)
	{
		dangerStrips[i]->getModels(list);
	}

	for (int i = 0; i< stripCount; ++i)
	{
		strips[i]->getModels(list);
	}
}

void LevelBlock::setDifficulty(float difficulty)
{
	//Schwierigkeit aller Objekte setzen
	this->difficulty = difficulty;
	for (int i = 0; i< dangerStripCount; ++i)
	{
		dangerStrips[i]->setDifficulty(difficulty + i * 0.3f);
	}
}

bool LevelBlock::checkCollision(Vector position, Vector boundingBox) {
	Collision c;
    if(c.checkCollision(Transform.translation() + this->boundingBox.size() * 0.5f, this->boundingBox.size(), position, boundingBox))
    {
        //Alle Spuren durchlaufen und auf Collision prüfen.
        for(int i = 0; i<dangerStripCount; ++i)
        {
            if((*dangerStrips[i]).checkCollision(position, boundingBox))
            {
                //Eine Kollision mit einem Gefahrenobjekt im Streifen ist aufgetreten.
                return true;
            }
        }
    }
    return false;
}

LevelBlock::~LevelBlock() {
    delete(facade);
	for (int i = 0; i < dangerStripCount; ++i)
	{
		delete dangerStrips[i];
	}
	for (int i = 0; i < stripCount; ++i)
	{
		delete strips[i];
	}
	delete[] dangerStrips;
	delete[]strips;
}
