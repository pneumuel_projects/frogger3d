#include "rgbimage.h"
#include "color.h"

RGBImage::RGBImage(unsigned int Width, unsigned int Height) {
	m_Image = new Color[Width * Height];
	m_Height = Height;
	m_Width = Width;
}

RGBImage::~RGBImage() {
		/*if (m_Image != nullptr) {
			delete (m_Image);
		}
		m_Image = nullptr;*/
}

void RGBImage::setPixelColor(unsigned int x, unsigned int y, const Color &c) {
	m_Image[y * m_Width + x] = c;
}

const Color &RGBImage::getPixelColor(unsigned int x, unsigned int y) const {
	return m_Image[y * m_Width + x];
}

unsigned int RGBImage::width() const {
	return m_Width;
}

unsigned int RGBImage::height() const {
	return m_Height;
}

unsigned char RGBImage::convertColorChannel(float v) {
	if (v <= 0.0f) { return 0; }
	if (v >= 1.0f) { return 255; };
	auto r = static_cast<unsigned char>(255 / 1.0f * v);
	return r;
}

bool RGBImage::saveToDisk(const char *Filename) {
	//Groesse des Pixelarrays bestimmen
	unsigned int arraySize = (m_Width * m_Height);
	//Groesse der Pixel Daten im Bitmap berechnen. (3Byte, 3 Byte, 2Byte f�r 4Byte Ausrichtung
	unsigned int filesize = arraySize * 3 + 54;

	auto *pixelData = new unsigned char[3 * arraySize];
	for (int x = 0; x < m_Width; ++x) {
		for (int y = m_Height - 1; y >= 0; --y) {
			pixelData[(x + y * m_Width) * 3 + 2] = convertColorChannel(m_Image[y * m_Width + x].R);
			pixelData[(x + y * m_Width) * 3 + 1] = convertColorChannel(m_Image[y * m_Width + x].G);
			pixelData[(x + y * m_Width) * 3 + 0] = convertColorChannel(m_Image[y * m_Width + x].B);
		}
	}

	unsigned char bmpfileheader[14] = { 'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0 };
	unsigned char bmpinfoheader[40] = { 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0 };
	unsigned char bmppad[3] = { 0, 0, 0 };

	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize >> 8);
	bmpfileheader[4] = (unsigned char)(filesize >> 16);
	bmpfileheader[5] = (unsigned char)(filesize >> 24);

	bmpinfoheader[4] = (unsigned char)(m_Width);
	bmpinfoheader[5] = (unsigned char)(m_Width >> 8);
	bmpinfoheader[6] = (unsigned char)(m_Width >> 16);
	bmpinfoheader[7] = (unsigned char)(m_Width >> 24);
	bmpinfoheader[8] = (unsigned char)(m_Height);
	bmpinfoheader[9] = (unsigned char)(m_Height >> 8);
	bmpinfoheader[10] = (unsigned char)(m_Height >> 16);
	bmpinfoheader[11] = (unsigned char)(m_Height >> 24);

	FILE *f = fopen(Filename, "wb");

	//Header schreiben:
	fwrite(bmpfileheader, 1, 14, f);
	fwrite(bmpinfoheader, 1, 40, f);

	for (int i = 0; i < m_Height; i++) {
		fwrite(&pixelData[m_Width * ((m_Height - i - 1) * 3)], 3, m_Width, f);
		fwrite(bmppad, 1, (4 - (m_Width * 3) % 4) % 4, f);
	}

	fclose(f);
	delete(pixelData);
	return true;
}
