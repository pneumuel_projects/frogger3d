#ifndef Collision_h
#define Collision_h

#include "Aabb.h"

class Collision
{
public:
	static bool checkCollision(Vector aPosition, Vector aBoundingBoxSize, Vector bPosition, Vector bBoundingBoxSize);
};

#endif /* Collision_h */