#ifndef DangerObject_h
#define DangerObject_h

#include "BaseModel.h"
#include "Model.h"
#include "PhongShader.h"
#include <list>

class DangerObject : public Model
{
public:
	DangerObject();
	DangerObject(const char* modelPath,Vector position ,float speed = 1.f);
	virtual ~DangerObject();

	virtual bool checkCollision(Vector position, Vector boundingBoxSize) const;
	virtual void update(float deltaTime);
	virtual void draw(const BaseCamera & Cam);
	virtual void setPosition(Vector position);
	virtual std::string& getPath() { return Filepath; }
	virtual float getSpeed() { return speed; }
	void setSpeed(float speed);
	void getModels(std::list<BaseModel*>& list);
protected:

private:
	float speed;
	Model* tireFrontRight;
	Model* tireFrontLeft;
	Model* tireBackRight;
	Model* tireBackLeft;
	SpotLight* leftCarLight;
	SpotLight* rightCarLight;
};

#endif /* DangerObject */