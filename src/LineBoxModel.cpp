//
//  LineBoxModel.cpp
//  CGXcode
//
//  Created by Philipp Lensing on 10.10.16.
//  Copyright © 2016 Philipp Lensing. All rights reserved.
//

#include "LineBoxModel.h"

LineBoxModel::LineBoxModel( float Width, float Height, float Depth )
{
	size = Vector(Width, Height, Depth);
}

void LineBoxModel::draw(const BaseCamera& Cam)
{
	float BeginX = size.X / 2;
	float BeginZ = size.Z / 2;
	float BeginY = size.Y / 2;
	Vector translation = Transform.translation();
	translation = Vector(translation.X, -translation.Z, translation.Y);
	VB.begin();
	//Boden
	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);

	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);

	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);

	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);

	//Decke
	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);

	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, BeginZ + +translation.Z);
	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);

	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, BeginZ + translation.Z);

	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);

	//Seiten
	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, BeginZ + translation.Z);

	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, BeginZ + translation.Z);

	VB.addVertex(-BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);
	VB.addVertex(-BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);

	VB.addVertex(BeginX + translation.X, -BeginY + translation.Y, -BeginZ + translation.Z);
	VB.addVertex(BeginX + translation.X, BeginY + translation.Y, -BeginZ + translation.Z);

	VB.end();

	//BaseModel::draw(Cam);

	VB.activate();

	glDrawArrays(GL_LINES, 0, VB.vertexCount());

	VB.deactivate();
}
