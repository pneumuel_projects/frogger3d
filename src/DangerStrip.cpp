#include "DangerStrip.h"
#include "Collision.h"

namespace fs = std::experimental::filesystem;

DangerStrip::DangerStrip() : Model(), dangerObjects(nullptr), dangerObjectCount(0), dangerObjectOffset(0)
{
	lastDangerObjectIndex = -1;
	difficulty = 0.f;
}

DangerStrip::DangerStrip(const char* blockPath, Vector position, float difficulty)
{
	std::cout << "\t\t|| DangerStrip" << std::endl;

	//Parameter übernehmen
	this->path = blockPath;
	this->lastDangerObjectIndex = 0;
	this->difficulty = difficulty;

	//Straße laden
	std::string laneModelPath= blockPath;
	laneModelPath = laneModelPath.append("floor.dae");
	load(laneModelPath.c_str() ,false);
	BaseModel::shader(new PhongShader(), true);
	shadowCaster(true);


	//Bounding vox verlängern für Collision detection
	Transform.translation(position + Vector(BoundingBox.size().X / 2, 0, 0));
	Vector bboXMax = BoundingBox.Max;
	bboXMax = Vector(300, bboXMax.Y, bboXMax.Z);
	BoundingBox.Max = bboXMax;

	//Die Autos laden
	std::string dangerPath = blockPath;
	dangerPath = dangerPath.append("dangers/");
	this->dangerObjects = new DangerObject*[255];
	dangerObjectCount = 0;

	//Ein Offset bestimmen um die Objekte zu verschieben:
	float LO = -15.f;
	float HI = 0.f;
	this->dangerObjectOffset = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));

	for (auto & p : fs::directory_iterator(dangerPath)) 
	{
		//Maximal 255 Objekte
		if (dangerObjectCount >= 255) 
		{
			break;
		}

		//Prüfen ob die Endung der Datei passend ist.
		std::string extension = p.path().extension().generic_string();
		if (extension == ".dae")
		{
			//Wenn ja, Model 3 mal laden
			for (int i = 0; i < 3; ++i) 
			{
				DangerObject* dangerObject = new DangerObject(p.path().generic_string().c_str(), position + Vector((5.f * i),0,0), DangerStrip::calculateSpeed());
				dangerObjects[dangerObjectCount++] = dangerObject;
			}
		}
		
	}

	if (dangerObjectCount == 0) 
	{
		return;
	}

	//Die Autos Mischen
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(&dangerObjects[0], &dangerObjects[dangerObjectCount-1], std::default_random_engine(seed));
	return;
}

DangerStrip::~DangerStrip()
{
	for (int i = 0; i < dangerObjectCount; ++i) 
	{
		delete dangerObjects[i];
	}
	delete[] dangerObjects;
}

bool DangerStrip::checkCollision(Vector position, Vector boundingBoxSize) const {
    Collision c;

	//Prüfen ob Objekt in eigener Bounding Box ist.
    if(c.checkCollision(Vector(0,0,this->Transform.translation().Z), BoundingBox.size(), position, boundingBoxSize))
    {
        //Alle Gefahrenobjekte durchlaufen und auf Kollision prüfen.
        for(int i = 0; i< dangerObjectCount; ++i)
        {
            if(dangerObjects[i]->checkCollision(position, boundingBoxSize))
            {
                //Eine Kollision mit einem Gefahrenobjekt ist aufgetreten.
                return true;
            }
        }
    }
    return false;
}

void DangerStrip::update(float deltaTime)
{
	Vector spawnpoint(this->dangerObjectOffset, 0, 0);
	spawnpoint += Vector(0, this->Transform.translation().Y, this->Transform.translation().Z);
	if (this->lastDangerObjectIndex >= 0)
	{
		//Prüfen ob das vorherige Objekt weit genug weg ist
		Vector distance = (dangerObjects[lastDangerObjectIndex]->transform().translation() - (dangerObjects[lastDangerObjectIndex]->boundingBox().center() - dangerObjects[lastDangerObjectIndex]->boundingBox().Min) - spawnpoint);
		if (distance.length() > 20.f)
		{
			//Das Objekt positionieren
			int nextIndex = (lastDangerObjectIndex + 1) % dangerObjectCount;

			Matrix m;
			m.translation(spawnpoint);
			dangerObjects[nextIndex]->transform(m);
			lastDangerObjectIndex = nextIndex;
		}
	}
	else 
	{
		Matrix m;
		m.translation(spawnpoint);
		dangerObjects[0]->transform(m);
		lastDangerObjectIndex = 0;
	}

	//Alle Gefahrenobjekte durchlaufen und aktualisieren
	for (int i = 0; i< dangerObjectCount; ++i)
	{
		dangerObjects[i]->update(deltaTime);
	}
}

void DangerStrip::draw(const BaseCamera & Cam)
{
	//Alle Gefahrenobjekte zeichnen
	for (int i = 0; i < this->dangerObjectCount; ++i) 
	{
		dangerObjects[i]->draw(Cam);
	}

	//Eigenes Model zeichnen
	Model::draw(Cam);
}

void DangerStrip::setPosition(Vector position)
{
	//Eigene Position und aller Gefahrenobjekte setzen
	Matrix m;
	m.translation(position);
	Transform = Transform * m;

	for(int i = 0; i < dangerObjectCount; ++i)
	{
		dangerObjects[i]->setPosition(position);
	}
}

void DangerStrip::setDifficulty(float difficulty)
{
	//Schwierigkeit setzen
	this->difficulty = difficulty;
	for (int i = 0; i < dangerObjectCount; ++i)
	{
		dangerObjects[i]->setSpeed(calculateSpeed());
	}
}

void DangerStrip::getModels(std::list<BaseModel*>& list)
{
	list.push_back(this);
	for (int i = 0; i< dangerObjectCount; ++i)
	{
		dangerObjects[i]->getModels(list);
	}
}

float DangerStrip::calculateSpeed()
{
	return  difficulty * 0.3f;
}
