#pragma once

#include <irrKlang.h>
#include <GL/glew.h>
#include <glfw/glfw3.h>


/*IRRKLANG LIB verwendet ambiera.com/irrklang */

const int ANZAHLSONGS = 3;	//Anzahl der Gesamten eingefuegten Songs

class Sound
{
	//Irrklang lib
	irrklang::ISoundEngine *SoundEngine;
	
public:
	
	//Konstruktor
	Sound();
	//Stopt die Musik
	void stopPlay();
	//change Musik
	void changeMusik(int i);
	//Soundeffekt 
	void playJump();
	//Soundeffekt Dead
	void playDead();
	//Soundvolume reduzieren
	void decreaseVolume();
	//Soundevolume erhoehen
	void increaseVolume();
	//Getter Setter
	void setCurrentMusic( const int i );
	int getCurrentMusic() const;
	//Getter Setter
	void setOldStateDown( const int i );
	int getOldStateDown() const;
	//Getter Setter
	void setOldStateUp(const int i);
	int getOldStateUp() const;
	//Getter Setter
	void setDeadSound(const int i);
	int getDeadSound() const;

private:

	//Membervariablen
	int currentMusic;
	int oldStateDown;
	int oldStateUp;
	int deadSound;
	int volume;
};