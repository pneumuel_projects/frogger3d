#ifndef PARTICLE_GENERATOR_H
#define PARTICLE_GENERATOR_H

/*Inspiration von github.com/usmanshaahid/ParticleSystem
wurde aber stark abgeandert*/

#include "vector.h"
#include <stdlib.h>
#include "Camera.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include "GLFW\glfw3.h"
#include "Model.h"

const int ANZAHLPARTICLE = 300;		//Anzahl an Partikeln
const float LAENGE = 2.0;			//Groe�e des Angezeigten Raums
const int MINVELOCITYINIT = 1;		//Minimale Geschwindikeit bei Initialisierung
const int MAXVELOCITYINIT = 2;		//Maximale Geschwindigkeit bei Initialisierung
const int MAXVELOCITY = 2;			//Maximale Geschwindkeit Gesamt
const int MINMASS = 100;			//Minimale Masse
const int MAXMASS = 500;			//Maximale Masse
const int MASSEREICHWEITE = 3000;	//Massenausdehnung
const int MAXIMALEPARTICLE = 10000;	//Anzahl an Maximalen PArtikeln (Auch nach Neuverzeilung)
const float DELTA = 1;

class ParticleGenerator
{
	//Membervariablen
	float mass;
	Vector velocity;
	Vector position;
	Vector initialPosition;
	
public:
	//Konstruktor
	ParticleGenerator(Vector position);
	//Konstruktor
	ParticleGenerator();
	//Konstruktor COPY
	ParticleGenerator(const ParticleGenerator &other);
	//Hilffunktion Random Float
	static float randFlow();
	//Random Verteilung der Parikel
	void verteilung(float, Vector force);
	//Getter f�r Position
	Vector getPosistion();
	//Destruktor
	~ParticleGenerator(void);
};

#endif /*PARTICLEGENERATOR.H*/