#include "Frog.h"
#include "PhongShader.h"

#ifdef WIN32
#define ASSET_DIRECTORY "../../assets/"
#else
#define ASSET_DIRECTORY "../assets/"
#endif

Frog::Frog()
{
	//Eigenes Model laden
	load(ASSET_DIRECTORY "frog.dae");
	BaseModel::shader(new PhongShader, true);
	Matrix m;
	m.translation(Vector(10.f, 0, 0));
	Transform = Transform * m;

	//Parameter setzen
	velocityX = 0;
	velocityY = 0;
	gravity = -25.f;
	alive = true;
	jumping = false;
	direction = FORWARD;
}


Frog::~Frog()
= default;

void Frog::kill() {
	alive = false;
	if (frogSound.getDeadSound() == 0) {
		frogSound.setDeadSound(1);
		frogSound.playDead();
	}
}

void Frog::revive() {
	alive = true;
	frogSound.setDeadSound(0);
}

void Frog::update(float deltaTime)
{
	//Sprung verarbeiten
	if (jumping)
	{
		Matrix jmpMatrix;

		//Die Forlem fpr den Sprung braucht gesamtzeit..
		totalTime += deltaTime;

		//H�he des Springes berechnen
		//Quelle der Formel leider nicht mehr auffindbar
		// s = v * t + 1/2 * g * t^2
		const float height = velocityY * totalTime + 0.5f * gravity * totalTime * totalTime;

		//Translation setzen, Sprungstrecke berechnen (vX * t)
		jmpMatrix.translation(0, height - lastJumpPosition.Y, velocityX * totalTime - lastJumpPosition.Z);
		
		lastJumpPosition = Vector(0, height, velocityX * totalTime);
		Transform = Transform * jmpMatrix;

		//Wenn der Frosch wieder auf dem Boden ist, Sprung beenden
		if (Transform.translation().Y <= 0)
		{
			jumping = false;
			velocityY = 0.f;
			velocityX = 0.f;
			Vector tmp = Transform.translation();
			tmp = Vector(tmp.X, 0, tmp.Z);
			Transform.translation(tmp);
		}
	}
}

void Frog::draw(const BaseCamera& Cam)
{
	Model::draw(Cam);
}

void Frog::jump(Direction direction, float distance)
{
	//Frosch kann nur springen wenn er auf dem Boden ist
	if (!jumping) {
		this->direction = direction;
		velocityY = 10.f;

		//Jump sound abspielen
		frogSound.playJump();

		//Um den Frosch eine bestimmte strecke h�pfen lassen zu k�nnen, muss die obige Formel umgestellt werden um die Sprumgzeit zu ermitteln.
		const float jumpTime = -2 * velocityY / gravity;
		
		//Die Sprunkgraft in X Richtung wird dann einfach aus der Sprungzeit und der Distanz berechnet.
		velocityX = distance / jumpTime;

		//Der Frosch je nach Sprungrichtung ausrichten
		Matrix rotateMatrix;
		switch (direction)
		{
		case FORWARD:
			rotateMatrix.rotationY(0);
			break;
		case LEFT:
			rotateMatrix.rotationY(1.570796f);
			break;
		case RIGHT:
			rotateMatrix.rotationY(-1.570796f);
			break;
		}

		//Variablen f�r Sprung zur�cksetzen
		lastJumpPosition = Vector(0, 0, 0);
		Transform = Transform * rotateMatrix;
		totalTime = 0;
		jumping = true;
	}
}
