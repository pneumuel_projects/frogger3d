#include "ParticleSystem.h"

/*Inspiration von github.com/usmanshaahid/ParticleSystem
wurde aber stark abgeandert*/

//Konstruktor
ParticleSystem::ParticleSystem()
{
	particles = std::vector<ParticleGenerator>();
	gravityPoint = Vector(0, 0, 0);
}

//Nachverteilung der Parikel, welche aus der Liste kommen
void ParticleSystem::nachVerteilung(float time)
{
	Vector force(10.0,-100.0,10.0);
	std::vector<ParticleGenerator>::iterator it;
	for (it = particles.begin(); it != particles.end(); it++)
	{
		Vector force = Vector((gravityPoint - it->getPosistion()).unit(), MASSEREICHWEITE);
		it->verteilung(time, force);
	}
}

//Setter Gravity
void ParticleSystem::setGravity(Vector gravity) {
	gravityPoint = gravity;
}

//Hinzufuegen von einzenlnen Paricle in die Liste
bool ParticleSystem::addParticlesPosition(int num, Vector position)
{
	int i;
	for (i = 0; i < num && particles.size() < MAXIMALEPARTICLE; i++)
	{
		ParticleGenerator p(position);
		particles.push_back(p);
	}
	return (i >= num);
}

//Loescht Parikel
bool ParticleSystem::deleteParticles(int num)
{
	int i;
	for (i = 0; i < num && particles.size() > 0; i++)
	{
		particles.pop_back();
	}

	return (i >= num);
}

void ParticleSystem::draw(Camera& cam)
{
	//GLPOINT draw color points
	std::vector<ParticleGenerator>::iterator it;
	for(int i = 0; i < particles.size(); ++i){
		Vector pos = particles.at(i).getPosistion();
		/*Matrix m = cam.getProjectionMatrix();
		m.invert();
		pos = m * pos;
		pos = (pos - cam.position());*/
		//pos.X = pos.X + cam.position().X;
		//pos.Y = pos.Y + cam.position().Y;
		//pos.Z = pos.Z + cam.position().Z;
		glPointSize(3);
		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_BLEND);
		glColor4f(1.0, 0.0, 0.0, 0.5);

		glBegin(GL_POINTS);
		glVertex3f(pos.X,pos.Y,pos.Z);
		glEnd();
		
	}
}

//calc3DRay aus Matrix.cpp (angepasst)
Vector ParticleSystem::calc3DRay(Vector particlePosition, Camera& cam)
{
	int width = 1920;
	int height = 1080;
	auto const newX = ((particlePosition.X * 2) / width) - 1;
	auto const newY = (((particlePosition.Y * 2) / height) - 1) * (-1);

	auto proj = cam.getProjectionMatrix();
	proj.invert();

	const auto dir = proj * Vector(newX, newY, particlePosition.Z);
	auto view = cam.getViewMatrix();
	view.invert();

	const auto newDir = view.transformVec3x3(dir);

	return newDir;
}

//Destructor
ParticleSystem::~ParticleSystem(void)
{
}